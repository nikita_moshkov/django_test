from rest_framework import serializers
from tictac.models import Game, Player


class PlayerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Player
        fields = ('id', 'username')


class GameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game
        fields = ('id', 'player_one', 'player_two', 'board')

    player_one = serializers.SerializerMethodField('get_player_username')
    player_two = serializers.SerializerMethodField('get_player_username')

    def get_player_username(self, game):
		return game.player.username