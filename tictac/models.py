# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Player(models.Model):

	username = models.CharField(max_length=255, unique=True)

	@staticmethod
	def get_or_create(username):
		try:
			return Player.objects.get(username=username)
		except Player.DoesNotExist:	
			return Player.objects.create(username=username)

class Game(models.Model):
	
	PLAYER_ONE = 'X'
	PLAYER_TWO = '0'


	player_one = models.ForeignKey(Player, related_name='one')
	player_two = models.ForeignKey(Player, related_name='two')
	board = models.CharField(max_length=9, default="NNNNNNNNN")

	def check_game_tie(self):
		for i in range(len(self.board)):
			if self.board[i] == 'N':
				return False

		return True


	def check_vicory(self):
		for i in range(0, 3):
			if ( (self.board[0+i*3] == self.board[1+i*3] == self.board[2+i*3]) and self.board[0+i*3] != 'N') or ( (self.board[0+i] == self.board[3+i] == self.board[6+i]) and self.board[0+i*3] != 'N'):
				return True

		if (self.board[0] == self.board[4] == self.board[8] and self.board[4] != 'N') or (self.board[2] == self.board[4] == self.board[6] and self.board[4] != 'N'):
			return True

		return False



