# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, PermissionDenied
from tictac.models import Player, Game
from tictac.serializers import GameSerializer, PlayerSerializer


@api_view(['POST'])
def new_game(request):

    username_player_one = request.data.get('username')
    username_player_two = request.data.get('username')

    player_one = Player.get_or_create(username_player_one)
    player_two = Player.get_or_create(username_player_two)
    game = Game.objects.create(player=player)
    serializer = GameSerializer(game)
    serializer_class = GameSerializer
    return Response(serializer.data)


@api_view(['POST'])
def play(request):
    username_player_one = request.data.get('username_one')
    username_player_two = request.data.get('username_two')
    serializer_class = GameSerializer
    game_id = request.data.get('game_id')
    position = request.data.get('position')
    #if not (username and game_id and type(position) is int):
    #   raise ValidationError('Missing username, game_id or position')

    player_one = get_object_or_404(Player, username=username_player_one)
    player_two = get_object_or_404(Player, username=username_player_two)
    game = get_object_or_404(Game, id=game_id)
    if game.player.id != player.id:
        raise PermissionDenied('Other player\'s game')

    game.play(position)
    serializer = GameSerializer(game)

    return Response(serializer.data)
